#!/usr/bin/python
#-*- coding: utf-8-*-
# exemple client es connecta i rep la info del servidor. Rep i plega
# @isx39441584
# Brian Mengibar Garcia
# Synopsis: 1repas-listUnits_server -s -p [-d]
#--------------------------------------------------------------------

import os,sys,socket,argparse,signal,time
from subprocess import Popen,PIPE

# Daemon
pid=os.fork()

if pid != 0:
	print "Programa PARE espera a que finalitzi el fill"
	sys.exit(0)
else:
	print "Programa fill comença el programa\n"

listClients = []
listIpDiff = []
ipDic = {}

# Signals

def handlerUsr1(signum, frame):
	global listClients
	print "Llistat de clients:"
	for el in ipDic:
		print "Client: %s Connections: %s" % (el, ipDic[el])

def handlerUsr2(signum,frame):
	global listClients
	print "Total de connexions:", len(ipDic.keys())

def handlerAlarm(signum, frame):
    print "s'ha acabat el teu temps!"
    sys.exit(0)

def handlerTerm(signum, frame):
	global ipDic
	print ipDic

def handlerHup (signum, frame):
	global listClients
	listClients = []

signal.signal(signal.SIGALRM, handlerAlarm)
signal.signal(signal.SIGUSR1, handlerUsr1)
signal.signal(signal.SIGUSR2, handlerUsr2)
signal.signal(signal.SIGTERM, handlerTerm)
signal.signal(signal.SIGHUP, handlerHup)
signal.signal(signal.SIGINT, signal.SIG_IGN)

# Creació arguments
parser = argparse.ArgumentParser(description="1repas-listUnits_server.py -s server -p port [-d]")
parser.add_argument("-s", "--server", dest="hostConnect", required=True)
parser.add_argument("-d", "--debug", dest="debugStdin", action="store_true")
parser.add_argument("-p", "--port", dest="portConnect", type=int, required= True) 

args = parser.parse_args()
HOST = args.hostConnect
PORT = args.portConnect
debug = args.debugStdin

#Inici del socket
s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
s.bind((HOST, PORT))
s.listen(1)
print os.getpid()
command=[]
# bucle infinit
while True:
	try:
		conn, addr = s.accept()
	except Exception:
		continue
	nom=addr[0]
	#nom="%s-%s" % (addr[0],time.strftime("%Y%m%d%H%M"))
	print "IP ",addr[0]
	if ipDic.has_key(nom):
		ipDic[nom]+=1
	else:
		ipDic[nom]=1
		
	print "Diccionario ",ipDic
	
	# Creació tuberia
	command = ['systemctl list-units']
	pipeData=Popen(command,shell=True,stdout=PIPE,stderr=PIPE)
	for line in pipeData.stdout:
		if debug: print line
		conn.send(line)
	conn.close()
sys.exit(0)
