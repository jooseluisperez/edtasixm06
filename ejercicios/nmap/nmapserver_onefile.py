#!/usr/bin/python 
#-*- coding: utf-8-*-
# exemple de nmap server amb un
# unic fitxer per client.
# José Luis Pérez
# -----------------------------------------------------------------
# Escola del treball de Barcelona
# ASIX Hisi2 M06-ASO UF2NF1-Scripts
# @edt Curs 2017-2018  Febrer 2018
# -----------------------------------------------------------------

import sys,socket, time, os
from subprocess import Popen, PIPE

#Definició de variables i constants.
HOST = ''
PORT = 50008
ipDic = {}

# Conexió (sockets).
s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
s.bind((HOST, PORT))
s.listen(1)                                                 

while True:

	try:
		conn, addr = s.accept()
	except Exception:
		continue

	print 'Connected by', addr
	
	filename = "%s.txt" % (addr[0])
	
	if not ipDic.has_key(addr[0]):
		ipDic[addr[0]]=filename
		ipList.append(ip)
	
	print ipDic
	ff = open(filename, 'a')

	while True:
		data = conn.recv(1024)
		if not data:
			ff.close()
			break
		ff.write(data)

	conn.close()

sys.exit(0)
