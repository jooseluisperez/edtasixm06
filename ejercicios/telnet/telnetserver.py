#!/usr/bin/python 
#-*- coding: utf-8-*-
# exemple de echo server basic
# -----------------------------------------------------------------
# Escola del treball de Barcelona
# ASIX Hisi2 M06-ASO UF2NF1-Scripts
# @edt Curs 2017-2018  Febrer 2018
# -----------------------------------------------------------------

import sys,socket, argparse, signal, os
from subprocess import Popen, PIPE

#Gestió d'arguments amb argparse
parser = argparse.ArgumentParser(description=\
 """Programa que emula un servidor telnet""",
 prog="telnetserver.py")
parser.add_argument("-d","--debug", dest="debug", action="store_true", help="debug mode")
parser.add_argument("-p","--port", dest="port", type=int, required=True, help="port")
args=parser.parse_args()


#Definició de variables i constants.
HOST = ''
PORT = args.port
EOT = chr(4)

# Conexió (sockets).
s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
s.bind((HOST, PORT))
s.listen(1)

while True:
    try:
        conn, addr = s.accept()
    except Exception:
        continue

    print 'Connected by', addr
    while True:
        command=conn.recv(1024)
        print command

        if not command: break

        pipeData = Popen(command, shell = True, stdout=PIPE, stderr=PIPE)
        for line in pipeData.stdout:
            if args.debug:
                print line,
            conn.sendall(line)
        conn.sendall(EOT)
    conn.close()
sys.exit(0)
