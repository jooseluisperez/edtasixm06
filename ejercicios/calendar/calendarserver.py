#!/usr/bin/python 
#-*- coding: utf-8-*-
# exemple de echo server basic
# -----------------------------------------------------------------
# Escola del treball de Barcelona
# ASIX Hisi2 M06-ASO UF2NF1-Scripts
# @edt Curs 2017-2018  Febrer 2018
# -----------------------------------------------------------------

import sys,socket, argparse, signal, os
from subprocess import Popen, PIPE

#Fork de programa pare y fill (daemon)
print "Inici del programa principal PARE"
pid = os.fork()
if pid != 0:
    print "Fi del programa principal PARE", os.getpid(), pid
    sys.exit(0)

print "Programa FILL", os.getpid(), pid

#Redefinició de Signals
def mysigusr1(signum,frame):
    global userList
    print "Signal handler called with signal:", signum
    print "Connected user IPs:\n", userList

def mysigusr2(signum,frame):
    global userList
    print "Signal handler called with signal:", signum
    print "Number of connected users:\n", len(userList)

def mysighup(signum,frame):
    global userList
    print "Signal handler called with signal:", signum
    print "Counters reseted:\n", userList, len(userList)

def mysigalarm(signum,frame):
    print "Signal handler called with signal:", signum
    print "Bye, bye!"
    sys.exit(0)

def mysigterm(signum,frame):
    print "Signal handler called with signal:", signum
    print "I won't close, muahahaha. "

signal.signal(signal.SIGALRM,mysigalarm)
signal.signal(signal.SIGTERM,mysigterm)
signal.signal(signal.SIGUSR1,mysigusr1)
signal.signal(signal.SIGUSR2,mysigusr2)
signal.signal(signal.SIGHUP,mysighup)

#Definició de variables i constants.
HOST = ''
PORT = 50005
userList = []

# Conexió (sockets).
s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
s.bind((HOST, PORT))
s.listen(1)

while True:
    try:
        conn, addr = s.accept()
        userList.append(addr)
    except Exception:
        continue
    print 'Connected by', addr
    year=conn.recv(1024)
    command = ["/usr/bin/cal -y %s" % year]
    pipeData = Popen(command, shell = True, stdout=PIPE, stderr=PIPE)
    for line in pipeData.stdout:
        #print line,
        conn.send(line)

conn.close()
sys.exit(0)
