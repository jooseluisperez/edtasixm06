# Apuntes Sistemas Operativos UF2



## Procesado de entrada y salida

### Procesado de estucturas

#### Listas

Los ejercicios de listas se encuentran en Nextcloud en la siguiente ruta: Escuela > CFGS > SO > UF1 > 01_PYTHON > 01-llistes-JL.

#### Tuplas

Los ejercicios de listas se encuentran en Nextcloud en la siguiente ruta: Escuela > CFGS > SO > UF1 > 01_PYTHON > 02-tuples-JL.

#### Diccionarios

Los ejercicios de listas se encuentran en Nextcloud en la siguiente ruta: Escuela > CFGS > SO > UF1 > 01_PYTHON > 03-ordenar_llistes-JL.

#### Ordenación

Los ejercicios de listas se encuentran en Nextcloud en la siguiente ruta: Escuela > CFGS > SO > UF1 > 01_PYTHON > 03-ordenar_llistes-JL.

#### List comprehensions

Los ejercicios de listas se encuentran en Nextcloud en la siguiente ruta: Escuela > CFGS > SO > UF1 > 01_PYTHON > 01-llistes-JL.


### Programación orientada a objetos

(POR ORDENAR Y ACABAR)

¿Que es una clase? Una idea de algo.  --> la idea de un objeto con dos ruedas y un manillar (bicicleta). El nombre comienza con una letra mayuscula.
	¿Que es un metodo? ejemplo: Como se hace algo, girar, acelerar, etc.. (una funcion)
	¿Que es una propiedad? Como es algo. Limpio, sucio, grande, etc.. El nombre empieza en minusculas pero si es compuesto la segunda palabra empieza en mayus.
¿Que es una instancia? Un ocurrencia del objecto concreto junto con sus propiedades.

Las clases tienen un constructor que es un mentodo que va con la primera letra en mayusculas i sirve para construir una clase. p=Popen(args) --> creamos un objeto popen que ejecutan subprocesos.
El constructor define el subprocess.


#### Clases

Una clase es una idea de algo, una "plantilla de objetos".

Por ejemplo, la idea de un objeto con dos ruedas, manillar y pedales (bicicleta). 

El nombre comienza con una letra mayuscula.

#### Objetos

Un objeto es una variable basada en una clase.

Para obtener un objeto tenemos que utilizar un constructor, este es un método pero con la primera letra en mayúsculas y se utiliza para contruir un objeto.

Por ejemplo, con "pastel=Pastelero(argumentos)" creamos un objeto "pastel" utilizando el contructor "Pastelero".

Cuando tenemos varios ejemplos de el mismo objeto, estos se denominan instancias.

#### Métodos

¿Que es un metodo? ejemplo: Como se hace algo, girar, acelerar, etc.. (una funcion)


### Procesado de argumentos con Argparse

hacer 7 ejercicios de argsparse


## IPC Inter Process Comunication

Interprocess Communication and Networking

No haremos: shared memory, semafors.

### Procesos del sistema

#### Ordenes y estuctura de los procesos

##### Ordenes básicas de gestión de procesos

> ps pstree pidof kill killall jobs fg bg

Ejemplos de ordenes basicas de procesos:

	[isx47240077@i04 ~]$ pstree -sp $(pgrep sleep)
	systemd(1)───systemd(1596)───tilix(13393)───bash(13397)───sleep(16300)

	[isx47240077@i04 ~]$ jobs
	[1]+  Ejecutando              sleep 1000 &

	[isx47240077@i04 ~]$ sleep 12345 &
	[2] 17359

	[isx47240077@i04 ~]$ jobs
	[1]-  Ejecutando              sleep 1000 &
	[2]+  Ejecutando              sleep 12345 &

	[isx47240077@i04 ~]$ fg %2
	sleep 12345
	^Z
	[2]+  Detenido                sleep 12345

	[isx47240077@i04 ~]$ jobs
	[1]-  Ejecutando              sleep 1000 &
	[2]+  Detenido                sleep 12345

	[isx47240077@i04 ~]$ bg
	[2]+ sleep 12345 &

	[isx47240077@i04 ~]$ jobs
	[1]-  Ejecutando              sleep 1000 &
	[2]+  Ejecutando              sleep 12345 &

	[isx47240077@i04 ~]$ killall sleep
	[1]-  Terminado               sleep 1000
	[2]+  Terminado               sleep 12345

	[isx47240077@i04 ~]$ lsof | wc -l
	64272

	[isx47240077@i04 ~]$ ldd /usr/bin/ls
		linux-vdso.so.1 (0x00007ffda27ad000)
		libselinux.so.1 => /lib64/libselinux.so.1 (0x00007f440200d000)
		libcap.so.2 => /lib64/libcap.so.2 (0x00007f4401e08000)
		libc.so.6 => /lib64/libc.so.6 (0x00007f4401a46000)
		libpcre.so.1 => /lib64/libpcre.so.1 (0x00007f44017d4000)
		libdl.so.2 => /lib64/libdl.so.2 (0x00007f44015d0000)
		/lib64/ld-linux-x86-64.so.2 (0x00007f4402454000)
		libattr.so.1 => /lib64/libattr.so.1 (0x00007f44013cb000)
		libpthread.so.0 => /lib64/libpthread.so.0 (0x00007f44011af000)

##### Estructura de los procesos

El directorio /proc es de tipo proc (es un sistema de ficheros de mentira)

	[root@i04 proc]# mount | grep proc
	proc on /proc type proc (rw,nosuid,nodev,noexec,relatime)
	systemd-1 on /proc/sys/fs/binfmt_misc type autofs (rw,relatime,fd=34,pgrp=1,timeout=0,minproto=5,maxproto=5,direct,pipe_ino=14650)
	nfsd on /proc/fs/nfsd type nfsd (rw,relatime)

Ejemplo de directorio de proceso:

	[root@i04 19088]# pwd
	/proc/19088

	[root@i04 19088]# ll
	total 0
	dr-xr-xr-x. 2 isx47240077 hisx2 0 Jan 30 11:55 attr
	-rw-r--r--. 1 isx47240077 hisx2 0 Jan 30 11:55 autogroup
	-r--------. 1 isx47240077 hisx2 0 Jan 30 11:55 auxv
	-r--r--r--. 1 isx47240077 hisx2 0 Jan 30 11:55 cgroup
	--w-------. 1 isx47240077 hisx2 0 Jan 30 11:55 clear_refs
	-r--r--r--. 1 isx47240077 hisx2 0 Jan 30 11:55 cmdline
	-rw-r--r--. 1 isx47240077 hisx2 0 Jan 30 11:55 comm
	-rw-r--r--. 1 isx47240077 hisx2 0 Jan 30 11:55 coredump_filter
	-r--r--r--. 1 isx47240077 hisx2 0 Jan 30 11:55 cpuset
	lrwxrwxrwx. 1 isx47240077 hisx2 0 Jan 30 11:55 cwd -> /home/users/inf/hisx2/isx47240077
	-r--------. 1 isx47240077 hisx2 0 Jan 30 11:55 environ
	lrwxrwxrwx. 1 isx47240077 hisx2 0 Jan 30 11:55 exe -> /usr/bin/sleep
	dr-x------. 2 isx47240077 hisx2 0 Jan 30 11:55 fd
	dr-x------. 2 isx47240077 hisx2 0 Jan 30 11:55 fdinfo
	-rw-r--r--. 1 isx47240077 hisx2 0 Jan 30 11:55 gid_map
	-r--------. 1 isx47240077 hisx2 0 Jan 30 11:55 io
	-r--r--r--. 1 isx47240077 hisx2 0 Jan 30 11:55 latency
	-r--r--r--. 1 isx47240077 hisx2 0 Jan 30 11:55 limits
	-rw-r--r--. 1 isx47240077 hisx2 0 Jan 30 11:55 loginuid
	dr-x------. 2 isx47240077 hisx2 0 Jan 30 11:55 map_files
	-r--r--r--. 1 isx47240077 hisx2 0 Jan 30 11:55 maps
	-rw-------. 1 isx47240077 hisx2 0 Jan 30 11:55 mem
	-r--r--r--. 1 isx47240077 hisx2 0 Jan 30 11:55 mountinfo
	-r--r--r--. 1 isx47240077 hisx2 0 Jan 30 11:55 mounts
	-r--------. 1 isx47240077 hisx2 0 Jan 30 11:55 mountstats
	dr-xr-xr-x. 7 isx47240077 hisx2 0 Jan 30 11:55 net
	dr-x--x--x. 2 isx47240077 hisx2 0 Jan 30 11:55 ns
	-r--r--r--. 1 isx47240077 hisx2 0 Jan 30 11:55 numa_maps
	-rw-r--r--. 1 isx47240077 hisx2 0 Jan 30 11:55 oom_adj
	-r--r--r--. 1 isx47240077 hisx2 0 Jan 30 11:55 oom_score
	-rw-r--r--. 1 isx47240077 hisx2 0 Jan 30 11:55 oom_score_adj
	-r--------. 1 isx47240077 hisx2 0 Jan 30 11:55 pagemap
	-r--------. 1 isx47240077 hisx2 0 Jan 30 11:55 personality
	-rw-r--r--. 1 isx47240077 hisx2 0 Jan 30 11:55 projid_map
	lrwxrwxrwx. 1 isx47240077 hisx2 0 Jan 30 11:55 root -> /
	-rw-r--r--. 1 isx47240077 hisx2 0 Jan 30 11:55 sched
	-r--r--r--. 1 isx47240077 hisx2 0 Jan 30 11:55 schedstat
	-r--r--r--. 1 isx47240077 hisx2 0 Jan 30 11:55 sessionid
	-rw-r--r--. 1 isx47240077 hisx2 0 Jan 30 11:55 setgroups
	-r--r--r--. 1 isx47240077 hisx2 0 Jan 30 11:55 smaps
	-r--------. 1 isx47240077 hisx2 0 Jan 30 11:55 stack
	-r--r--r--. 1 isx47240077 hisx2 0 Jan 30 11:55 stat
	-r--r--r--. 1 isx47240077 hisx2 0 Jan 30 11:55 statm
	-r--r--r--. 1 isx47240077 hisx2 0 Jan 30 11:55 status
	-r--------. 1 isx47240077 hisx2 0 Jan 30 11:55 syscall
	dr-xr-xr-x. 3 isx47240077 hisx2 0 Jan 30 11:55 task
	-r--r--r--. 1 isx47240077 hisx2 0 Jan 30 11:55 timers
	-rw-rw-rw-. 1 isx47240077 hisx2 0 Jan 30 11:55 timerslack_ns
	-rw-r--r--. 1 isx47240077 hisx2 0 Jan 30 11:55 uid_map
	-r--r--r--. 1 isx47240077 hisx2 0 Jan 30 11:55 wchan

Dentro del fichero environ podemos encontrar todas las variables de entorno que hereda el proceso de su padre (la que hay definidas como variables del sistema). Podemos comprobarlo ejecutando el comando env.

    joluper@lefto:/proc/1987$ cat environ 
    USER=joluperLANGUAGE=es_ESTEXTDOMAIN=im-configXDG_SEAT=seat0XDG_SESSION_TYPE=x11SSH_AGENT_PID=1800SHLVL=0QT4_IM_MODULE=ximHOME=/home/joluperDESKTOP_SESSION=budgie-desktopQT_STYLE_OVERRIDE=GTK_MODULES=gail:atk-bridgeXDG_SEAT_PATH=/org/freedesktop/DisplayManager/Seat0DBUS_SESSION_BUS_ADDRESS=unix:path=/run/user/1000/busQT_QPA_PLATFORMTHEME=gtk2IM_CONFIG_PHASE=2LOGNAME=joluperGTK_IM_MODULE=ibusXDG_SESSION_ID=c2PATH=/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin:/usr/games:/usr/local/games:/snap/binGDM_LANG=es_ESXDG_RUNTIME_DIR=/run/user/1000XDG_SESSION_PATH=/org/freedesktop/DisplayManager/Session0DISPLAY=:0LANG=es_ES.UTF-8XDG_CURRENT_DESKTOP=Budgie:GNOMEXDG_SESSION_DESKTOP=budgie-desktopXMODIFIERS=@im=ibusXAUTHORITY=/home/joluper/.XauthorityXDG_GREETER_DATA_DIR=/var/lib/lightdm-data/joluperSSH_AUTH_SOCK=/run/user/1000/keyring/sshSHELL=/bin/bashQT_ACCESSIBILITY=1GDMSESSION=budgie-desktopTEXTDOMAINDIR=/usr/share/locale/XDG_VTNR=7QT_IM_MODULE=ibusPWD=/home/joluperCLUTTER_IM_MODULE=ximXDG_DATA_DIRS=/usr/share/budgie-desktop:/usr/share/budgie-desktop:/usr/local/share:/usr/share:/var/lib/snapd/desktopXDG_CONFIG_DIRS=/etc/xdg/xdg-budgie-desktop:/etc/xdg/xdg-budgie-desktop:/etc/xdgGNOME_DESKTOP_SESSION_ID=this-is-deprecatedXDG_MENU_PREFIX=gnome-SESSION_MANAGER=local/lefto:@/tmp/.ICE-unix/1680,unix/lefto:/tmp/.ICE-unix/1680DESKTOP_AUTOSTART_ID=10bafe7c55cf656b38151844376790659900000016800000GIO_LAUNCHED_DESKTOP_FILE=/usr/share/applications/budgie-wm.desktopGIO_LAUN


Dentro de cmdline tenemos el texto plano del comando que hemos ejecutado.
    
    joluper@lefto:/proc/1987$ cat /proc/3509/cmdline 
    sleep1000

###### Entradas de los procesos

	[root@i04 19088]# pwd
	/proc/19088

	[root@i04 19088]# ll fd
	fd/     fdinfo/ 

	[root@i04 19088]# ll fd/
	0  1  2 

0 = stdin (entrada estándard)

1 = stdout (salida estándard)

2 = stderr (salida de errores estándard)

Podemos ver que dentro del directorio fd/ tenemos los ficheros 0,1,2 que son la entrada estandard del sistema.

	[root@i04 19088]# ll /dev/std*
	lrwxrwxrwx. 1 root root 15 Jan 30 08:55 /dev/stderr -> /proc/self/fd/2
	lrwxrwxrwx. 1 root root 15 Jan 30 08:55 /dev/stdin -> /proc/self/fd/0
	lrwxrwxrwx. 1 root root 15 Jan 30 08:55 /dev/stdout -> /proc/self/fd/1

	[root@i04 19088]# ls -la fd/
	total 0
	dr-x------. 2 isx47240077 hisx2  0 Jan 30 11:55 .
	dr-xr-xr-x. 9 isx47240077 hisx2  0 Jan 30 11:54 ..
	lrwx------. 1 isx47240077 hisx2 64 Jan 30 12:05 0 -> /dev/pts/1
	lrwx------. 1 isx47240077 hisx2 64 Jan 30 12:05 1 -> /dev/pts/1
	lrwx------. 1 isx47240077 hisx2 64 Jan 30 12:05 2 -> /dev/pts/1

Podemos ver que dichos ficheros no son más que un enlace simbólico hacia la las entradas por defecto del sistema.

	[isx47240077@i04 ~]$ cat > /tmp/hola.txt
	(eperando entada)

	... (a la misma vez) ...

	[root@i04 19088]# ls -la /proc/24283/fd/
	total 0
	dr-x------. 2 isx47240077 hisx2  0 Jan 30 12:13 .
	dr-xr-xr-x. 9 isx47240077 hisx2  0 Jan 30 12:12 ..
	lrwx------. 1 isx47240077 hisx2 64 Jan 30 12:13 0 -> /dev/pts/4
	l-wx------. 1 isx47240077 hisx2 64 Jan 30 12:13 1 -> /tmp/hola.txt
	lrwx------. 1 isx47240077 hisx2 64 Jan 30 12:13 2 -> /dev/pts/4

En este caso comprobamos que si creamos un proceso en una terminal que tome por salida un fichero en vez de la pantalla (estándard) vemos que cambia en enlace simbólico.

#### Pipes

##### Ejemplo básico de procesos con pipes

En una terminal ejecutamos el siguiente comando con pipes.

	[isx47240077@i04 ~]$ cat | sort | wc -l > /tmp/sortida.txt
	(esperando entada)
	
.

	--inciso de info de los procesos--

	27868 pts/5    S+     0:00 cat
	27869 pts/5    S+     0:00 sort
	27870 pts/5    S+     0:00 wc -l
	28035 pts/1    R+     0:00 ps ax

En otra terminal (la primera terminal está esperando entrada) nos dirigimos a los directorios de los procesos creados y comprobamos sus entradas.

	[root@i04 19088]# ls -la /proc/27868/fd/
	total 0
	dr-x------. 2 isx47240077 hisx2  0 Jan 30 12:26 .
	dr-xr-xr-x. 9 isx47240077 hisx2  0 Jan 30 12:26 ..
	lrwx------. 1 isx47240077 hisx2 64 Jan 30 12:27 0 -> /dev/pts/5
	l-wx------. 1 isx47240077 hisx2 64 Jan 30 12:27 1 -> 'pipe:[319510]' #la salida del cat es el pipe 319510
	lrwx------. 1 isx47240077 hisx2 64 Jan 30 12:26 2 -> /dev/pts/5

	[root@i04 19088]# cat /proc/27868/cmdline 
	cat[root@i04 19088]#

	[root@i04 19088]# ls -la /proc/27869/fd/
	total 0
	dr-x------. 2 isx47240077 hisx2  0 Jan 30 12:26 .
	dr-xr-xr-x. 9 isx47240077 hisx2  0 Jan 30 12:26 ..
	lr-x------. 1 isx47240077 hisx2 64 Jan 30 12:28 0 -> 'pipe:[319510]' #la entrada del sort es el pipe 319510
	l-wx------. 1 isx47240077 hisx2 64 Jan 30 12:28 1 -> 'pipe:[319512]' #la salida del sort es el pipe 319512
	lrwx------. 1 isx47240077 hisx2 64 Jan 30 12:26 2 -> /dev/pts/5

	[root@i04 19088] #cat /proc/27869/cmdline 
	sort[root@i04 19088]# 

	[root@i04 19088]# ls -la /proc/27870/fd/
	total 0
	dr-x------. 2 isx47240077 hisx2  0 Jan 30 12:26 .
	dr-xr-xr-x. 9 isx47240077 hisx2  0 Jan 30 12:26 ..
	lr-x------. 1 isx47240077 hisx2 64 Jan 30 12:29 0 -> 'pipe:[319512]' #la entrada del wc es el pipe 319512
	l-wx------. 1 isx47240077 hisx2 64 Jan 30 12:29 1 -> /tmp/sortida.txt #la salida del fichero es fichero
	lrwx------. 1 isx47240077 hisx2 64 Jan 30 12:26 2 -> /dev/pts/5

	[root@i04 19088]# cat /proc/27870/cmdline 
	wc-l[root@i04 19088]# 

##### Named Pipes

Nos dirijimoms a un directorio de pruebas creamos con mkfifo el pipe dades.

	[root@i04 m06]# pwd
	/tmp/m06

	[root@i04 m06]# mkfifo dades

	[root@i04 m06]# ll
	total 0
	prw-r--r--. 1 root root 0 Jan 30 12:45 dades

Podemos ver que es un fichero de tipo pipe.

	[root@i04 m06]# sort < \/tmp/m06/dades
	(eperando entada)

Por una terminal del pipe ponemos a escuchar a sort (este se convierte en un extremo del pipe).

	[root@i04 ~]# cat /etc/fstab > /tmp/m06/dades (printa fstab en la otra consola)

Por el otro extremo del pipe (en otra terminal) volcamos el fichero fstab y vemos que nos lo printa ordenado por stdout en la consola del sort.

##### Popen (pipe open)

> Consultar documentación subprocess Python: https://docs.python.org/3/library/subprocess.html

Ejercicios de ejemplo con Popen

###### Ejemplo Popen básico

	[isx47240077@i04 ipc]$ vim 03-popen.py
	#!/usr/bin/python
	# joseluisperez
	# exemple popen harcoded
	# -----------------------

	import sys
	from subprocess import Popen, PIPE
	command=["who"]
	pipeData=Popen(command, stdout=PIPE, stderr=PIPE)
	for line in pipeData.stdout:
		print line

	sys.exit(0)

Este ejemplo simplemente abre un pipe y ejecuta un comando WHO y muere.

###### Ejecución de consultas contra Postgres con Popen

**Ejercicio 03a-pipeSql.py**

En el siguiente ejemplo se envía por el pipe una una sentencia que será ejecutada y leeremos el resultado por la salida el pipe linea a linea. 

    #!/usr/bin/python
    # @edt ASIX-M06 Curs 2017-2018
    # Popen amb consulta SQL 
    #   a) tot hardcoded
    #
    # Utilitza el docker edtasixm06/postgres
    # -----------------------------------------
    import sys #importamos la librería sys.
    from subprocess import Popen, PIPE #de la librería subprocess importamos las funciones Popen y PIPE.
    
    commandLocal = "psql -qtA -F ';' lab_clinic \
                         -c 'select * from pacients;'" #definimos la el acceso y query en caso de ejecución local.
    commandRemote = "psql -h 172.17.0.2 -U postgres  training \
                          -c 'select *  from oficinas;'" #definimos el acceso y query en caso de ejecución remota.
    pipeData = Popen(commandRemote, shell = True, stdin=PIPE, stdout=PIPE, stderr=PIPE) #abrimos el pipe con el atributo \
    shell=True para que lo utilice como interprete a la hora de ejecutar la query que hemos definido previamente. 
    for line in pipeData.stdout.readlines(): #por cada linea que lee del stdout
      print line, #muestra dicha linea
    sys.exit(0)
    
    
**Ejercicio 03b-pipeSql.py**

En el siguiente ejemplo enviamos una sentencia por el pipe que será ejecutada, esta solo abre la conexión conla base de datos. Posteriormente, escribiremos por la entrada del pipe una segunda sentencia que será ejecutada y leeremos el resultado de esta por la salida del pipe.
    
    #!/usr/bin/python
    # @edt ASIX-M06 Curs 2017-2018
    # Popen amb consulta SQL 
    #   a) tot hardcoded
    #   b) dialeg procés pare -subproces
    #
    # Utilitza el docker edtasixm06/postgres
    # -----------------------------------------
    import sys
    from subprocess import Popen, PIPE
    
    commandLocal = "psql -qtA -F ';' lab_clinic \
                         -c 'select * from pacients;'" #definimos la el acceso y query en caso de ejecución local.
    #commandRemote = "psql -qtA -h i11 -U postgres -F ';' lab_clinic \
    #                      -c 'select * from pacients;'"
    cmd = "psql -h 172.17.0.2 -U edtasixm06  training" #definimos el acceso en caso de ejecución remota.
    sqlStatment="select * from oficinas;\n\q\n" #definimos la query a ejecutar posteriormente.
    pipeData = Popen(cmd, shell = True, stdin=PIPE, stdout=PIPE, stderr=PIPE)  #abrimos el pipe con el atributo \
    shell=True para que lo utilice como interprete a la hora de ejecutar la query que hemos definido previamente.
    pipeData.stdin.write(sqlStatment) #escribimos la query a ejecutar.
    
    for line in pipeData.stdout: #por cada linea que lee del stdout
      print line, #muestra dicha linea
    sys.exit(0)
    
**Ejercicio 03c-pipeSql.py**

En el siguiente ejemplo, haciendo uso de argparser, recibimos la query por el argumento 'sqlStatment' pero tenemos por defecto uno establecido en SQLSTATMENT. Posteriormente realizamos la ejecución del comando de aptertura y la escritura de la query insertada tal y como hemos definido previamente.

    #!/usr/bin/python
    #-*- coding: utf-8-*-
    # @edt ASIX-M06 Curs 2017-2018
    # Popen amb consulta SQL 
    #   a) tot hardcoded
    #   b) diàleg proces pare - subprocés
    #   c) SQL INJECTAT -----> FATAL!!!! provar:
    #      select * from oficinas;drop table oficinas;
    #
    # Utilitza el docker edtasixm06/postgres
    # -----------------------------------------
    #commandLocal = "psql -qtA -F ';' lab_clinic -c 'select * from pacients;'"
    #commandRemote = "psql -qtA -h i11 -U postgres -F ';' lab_clinic -c 'select * from pacients;'"
    # -------------------------------------------
    import sys
    from subprocess import Popen, PIPE
    import argparse
    
    SQLSTATMENT="select * from oficinas;" #definimos la query por defecto en caso que no se haya insertado una en argparser.
    
    parser = argparse.ArgumentParser(description='Consulta SQL interactiva')
    parser.add_argument('sqlStatment', help='Sentència SQL a executar',metavar='sentènciaSQL', default=SQLSTATMENT)
    args = parser.parse_args() #evaluamos la situación de los campos
    
    cmd = "psql -h 172.17.0.2 -U postgres  training" #definimos el acceso en caso de ejecución remota.
    pipeData = Popen(cmd, shell = True, stdin=PIPE, stdout=PIPE, stderr=PIPE) #abrimos el pipe con el atributo \
    shell=True para que lo utilice como interprete a la hora de ejecutar la query que hemos definido previamente.
    pipeData.stdin.write(args.sqlStatment+"\n\q\n") #escribimos la query a ejecutar.
    
    for line in pipeData.stdout: #por cada linea que lee del stdout
      print line, #muestra dicha linea
    sys.exit(0)
    
**Ejercicio 03d-pipeSql.py**
    
En el siguiente ejemplo recogemmos como argumento con argparser el numero de empleado con el que queremos ejecutar la query de la misma forma que en el ejercico anterior. Posteriormente realizamos la ejecución del comando de aptertura y la escritura de la query insertada tal y como hemos definido previamente.
    
    #!/usr/bin/python
    #-*- coding: utf-8-*-
    # @edt ASIX-M06 Curs 2017-2018
    # Popen amb consulta SQL 
    #   a) tot hardcoded
    #   b) diàleg procés pare - subprocés
    #   c) SQL INJECTAT -----> FATAL!!!!
    #   d) consulta per num_empl INJECTAT FATAL!!! proveu:
    #       "112;drop table repventas;"
    #
    # Utilitza el docker edtasixm06/postgres
    # -----------------------------------------
    #commandLocal = "psql -qtA -F ';' lab_clinic -c 'select * from pacients;'"
    #commandRemote = "psql -qtA -h i11 -U postgres -F ';' lab_clinic -c 'select * from pacients;'"
    # -------------------------------------------
    import sys
    from subprocess import Popen, PIPE
    import argparse
    
    parser = argparse.ArgumentParser(description='Consulta SQL interactiva')
    parser.add_argument('numempl', help='Sentència SQL a executar',metavar='sentènciaSQL')
    args = parser.parse_args()
    
    cmd = "psql -h 172.17.0.2 -U postgres  training"
    sqlStatment="select * from repventas where num_empl=%s;" % (args.numempl)
    
    pipeData = Popen(cmd, shell = True, stdin=PIPE, stdout=PIPE, stderr=PIPE)
    pipeData.stdin.write(sqlStatment+"\n\q\n")
    
    for line in pipeData.stdout:
      print line,
    sys.exit(0)
    
**Ejercicio 04-multiSql.py**
    
Este ejemplo resulta basicamente idéntico al anteriror pero admitiendo múltiples números de usuario a la hora recoger los argumentos con argparse. De este modo significa que se ejecuta y muestra una query por número de usuario de la misma forma que lo hemos hecho anteriormente.
    
    #!/usr/bin/python
    #-*- coding: utf-8-*-
    # @edt ASIX-M06 Curs 2017-2018
    # Popen amb consulta SQL 
    # Amb diàleg pare - subproces per demanar client a client
    #
    # Utilitza el docker edtasixm06/postgres
    # -----------------------------------------
    import sys
    from subprocess import Popen, PIPE
    import argparse
    
    listClie=None
    parser = argparse.ArgumentParser(description='Consulta SQL interactiva')
    parser.add_argument("-c","--client", help='codi num_clie', metavar='NUM_CLIE', action='append',dest='listClie')
    args = parser.parse_args()
    print args
    
    cmd = "psql -qt -h 172.17.0.2 -U postgres training"
    pipeData = Popen(cmd, shell = True, stdin=PIPE, stdout=PIPE, stderr=PIPE)
    
    for clie in args.listClie:
      sqlStatment="select * from clientes where num_clie=%s;\n" % (clie)
      pipeData.stdin.write(sqlStatment)
      data=pipeData.stdout.readline()
      if len(data) != 1:
        print data,
        data=pipeData.stdout.readline()
      else:
        sys.stderr.write("clie:%s not found\n" % (clie))
    pipeData.stdin.write("\q\n")
    sys.exit(0)

#### Signals

Toda acción de parada/arranque/pausa/etc de un proceso se realiza enviando signals a estos que, en el caso que no se hayan modificado para realizar otra acción, actuan en base a una definición por defecto.

Podemos consultar todas las signals que tiene el sistema ejecutando el siguiente comando:

	[root@i04 ~]# kill -l
	 1) SIGHUP	 2) SIGINT	 3) SIGQUIT	 4) SIGILL	 5) SIGTRAP
	 6) SIGABRT	 7) SIGBUS	 8) SIGFPE	 9) SIGKILL	10) SIGUSR1
	11) SIGSEGV	12) SIGUSR2	13) SIGPIPE	14) SIGALRM	15) SIGTERM
	16) SIGSTKFLT	17) SIGCHLD	18) SIGCONT	19) SIGSTOP	20) SIGTSTP
	21) SIGTTIN	22) SIGTTOU	23) SIGURG	24) SIGXCPU	25) SIGXFSZ
	26) SIGVTALRM	27) SIGPROF	28) SIGWINCH	29) SIGIO	30) SIGPWR
	31) SIGSYS	34) SIGRTMIN	35) SIGRTMIN+1	36) SIGRTMIN+2	37) SIGRTMIN+3
	38) SIGRTMIN+4	39) SIGRTMIN+5	40) SIGRTMIN+6	41) SIGRTMIN+7	42) SIGRTMIN+8
	43) SIGRTMIN+9	44) SIGRTMIN+10	45) SIGRTMIN+11	46) SIGRTMIN+12	47) SIGRTMIN+13
	48) SIGRTMIN+14	49) SIGRTMIN+15	50) SIGRTMAX-14	51) SIGRTMAX-13	52) SIGRTMAX-12
	53) SIGRTMAX-11	54) SIGRTMAX-10	55) SIGRTMAX-9	56) SIGRTMAX-8	57) SIGRTMAX-7
	58) SIGRTMAX-6	59) SIGRTMAX-5	60) SIGRTMAX-4	61) SIGRTMAX-3	62) SIGRTMAX-2
	63) SIGRTMAX-1	64) SIGRTMAX

(INVESTIGAR QUE SIGNIFICAN CADA SEÑAL)

! TIP

	^S --> stop en bash
	
	^Q --> arranca 

##### Ejemplo signals básico

	#!/usr/sbin/python
	#capsalera
	#
	# Exemple IPC Signals
	# --------------------

	import signal, os, sys
	def myhandler(signum,frame):
	    print "Signal handler called with signal:", signum
	    print "hasta leugo lucas"
	    sys.exit(1)

	def mydeath(signum,frame):
	    print "Signal handler called with signal:", signum
	    print "que te mueras tu"

	signal.signal(signal.SIGALRM,myhandler) #"sobreescribimos" la acción que realiza SIGALRM por la que hemos definido en myhandler.
	signal.signal(signal.SIGTERM,mydeath)myhandler #"sobreescribimos" la acción que realiza SIGTERM por la que hemos definido en mydeath.
	signal.alarm(30) #definimos la alarma con 30 segundos.

	while True:
	    # Fem un open que es queda encallat (bucle infinit).
	    pass

	signal.alarm(0) #devuelve los segundos restantes.
	sys.exit(0)

##### Ejemplo signals 

(POR ACABAR Y EXPLICAR)

	#!/usr/bin/python
	# @edt ASSIX-M06 Curs 2017-2018
	#
	# Exemple IPC: Signals
	# ------------------------------------

	#prog minuts
	#alarma(n)
	#SIGHUP vuelve a los n segundos
	#SIGUSR1 aumenta el tiempo de alarma 1min
	#SIGUSR2 disminuye el tiempo de alarma 1min+
	#SIGTERM muestra los segundos que queda para que se acabe
	#NO se cierra con ^C
	#plega cuando acaba el tiempo o cuando se le manda SIGKILL
	#cuando plega muestra numero de uppers y downs


	import sys, os, signal
	from subprocess import Popen, PIPE
	import argparse

	parser = argparse.ArgumentParser(description='Gestionar alarma')
	parser.add_argument("minuts", type=int, help='Minuts per la alarma')
	args = parser.parse_args()
	print parser
	print os.getpid()


	global upper
	global down
	upper=0
	down=0

	def sighupHandler(signum,frame):
	  print "Signal handler called with signal:", signum
	  signal.alarm(args.minuts*60)

	def sigusr1Handler(signum,frame):
	  print "Signal handler called with signal:", signum
	  global upper
	  upper+=1
	  segons=signal.alarm(0)
	  signal.alarm(segons+60)
	  print segons

	def sigusr2Handler(signum,frame):
	  print "Signal handler called with signal:", signum
	  global down
	  down+=1
	  segons=signal.alarm(0)
	  signal.alarm(segons-60)
	  print segons

	def sigalarmHandler(signum,frame):
	  print "Signal handler called with signal:", signum
	  segons=signal.alarm(0)
	  signal.alarm(segons)
	  print segons
	  sys.exit(0)

	def sigtermHandler(signum,frame):
	  global upper, down
	  print "Signal handler called with signal:", signum
	  print args.minuts, signal.alarm(0), upper, down
	  print segons
	  sys.exit(0)

	signal.signal(signal.SIGALRM,sigalarmHandler)
	signal.signal(signal.SIGHUP,sighupHandler)
	signal.signal(signal.SIGUSR1,sigusr1Handler)
	signal.signal(signal.SIGUSR2,sigusr2Handler)
	signal.signal(signal.SIGTERM,sigtermHandler)
	signal.signal(signal.SIGINT, signal.SIG_IGN)
	signal.alarm(args.minuts*60)

	while True:
	  # fem un open que es queda encallat
	  pass

	signal.alarm(0)
	sys.exit(0)

#### Fork / execv 

https://docs.python.org/2/library/os.html
https://docs.python.org/2/library/signal.html
https://docs.python.org/2/library/ipc.html

Ejemplo:

	Libreoffice = proceso padre
		documento writer = proceso hijo
		documento writer = proceso hijo

##### Fork de procesos

###### Ejemplos de fork

	#!/usr/bin/python
	#pansalera
	#--------------------

	import os,sys

	print "Inici del programa principal PARE"
	pid = os.fork()
	if pid != 0:
		print "Programa PARE", os.getpid(), pid
	else:
		print "Programa FILL",os.getpid(), pid

	print "Fi del programa", os.getpid()
	sys.exit(0)

Tenemos un programa en el que en un momento se ejecuta la funcion os.fork() que retorna algo que se le asigna a la variable pid.
 
Esto provoca que el proceso de ejecución del programa se duplique, el proceso duplicado tiene las mismas instrucciones) con otro pid.

La función os.fork() retorna a la variable pid retorna:

- en el proceso padre el pid del proceso hijo 

- 0 en el proceso hijo (de esta forma el proceso hijo sabe que es hijo)


Este ejemplo es idéntico al anterior pero con os.wait() que hace que el proceso padre se espere que acabe el proceso hijo.

	#!/usr/bin/python
	#pansalera
	#--------------------

	import os,sys

	print "Inici del programa principal PARE"
	pid = os.fork()
	if pid != 0:
		os.wait()
		print "Programa PARE", os.getpid(), pid
	else:
		print "Programa FILL",os.getpid(), pid

	print "Fi del programa", os.getpid()
	sys.exit(0)

	#!/usr/bin/python
	#pansalera
	#--------------------

	import os,sys

	print "Inici del programa principal PARE"
	pid = os.fork()
	if pid != 0:
		print os.wait()
		print "Programa PARE", os.getpid(), pid
	else:
		print "Programa FILL",os.getpid(), pid

	print "Fi del programa", os.getpid()
	sys.exit(0)

##### Función os.execv()

(EXPLICACIÓN DE os.exec*())

###### Ejemplos básicos de os.execv()

En este ejemplo con os.execv("/usr/bin/ls",["/usr/bin/ls","-la","/"]) le pasamos la orden a ejecutar al proceso hijo.

El proceso hijo se redefine con la orden a ejecutar que le pasamos.

En los argumentos de os.execv() (["/usr/bin/ls","-la","/"]) hay que pasarle como primer campo de la lista el programa (otra vez). No es obligatorio pero es necesario.

	#!/usr/bin/python
	#pansalera
	#--------------------

	import os,sys

	print "Inici del programa principal PARE"
	pid = os.fork()
	if pid != 0:
		#print os.wait()
		print "Programa PARE", os.getpid(), pid
	else:
		print "Programa FILL",os.getpid(), pid
		os.execv("/usr/bin/ls",["/"]) #asi no funciona correctamente

	print "Fi del programa", os.getpid()
	sys.exit(0)

	#!/usr/bin/python
	#pansalera
	#--------------------

	import os,sys

	print "Inici del programa principal PARE"
	pid = os.fork()
	if pid != 0:
	    #print os.wait()
	    print "Programa PARE", os.getpid(), pid
	else:
	    print "Programa FILL",os.getpid(), pid
	    os.execv("/usr/bin/ls",["/usr/bin/ls","-la","/"]) # asi funciona correctamente

	print "Fi del programa", os.getpid()
	sys.exit(0)

Se utiliza execv o execl depende de como se quiera gestionar los argumentos (como una lista o como una cadena).

	#!/usr/bin/python
	#pansalera
	#--------------------

	import os,sys

	print "Inici del programa principal PARE"
	pid = os.fork()
	if pid != 0:
	    #print os.wait()
	    print "Programa PARE", os.getpid(), pid
	else:
	    print "Programa FILL",os.getpid(), pid
	    os.execl("/usr/bin/ls","/usr/bin/ls","-la","/") #con execl los argumentos de la lista pasan a ser argumentos de la función.

	print "Fi del programa", os.getpid()
	sys.exit(0)

Con la opción p de os.exec*() utiliza el path a la hora de ejecutar la orden y podemos poder la orden sin necesidad de la ruta absoluta.

	#!/usr/bin/python
	#pansalera
	#--------------------

	ejemplo fork y signal

	import os,sys

	print "Inici del programa principal PARE"
	pid = os.fork()
	if pid != 0:
	    #print os.wait()
	    print "Programa PARE", os.getpid(), pid
	else:
	    print "Programa FILL",os.getpid(), pid
	    os.execvp("ls",["ls","-la","/"]) # podemos poder la orden sin necesidad de la ruta absoluta

	print "Fi del programa", os.getpid()
	sys.exit(0)

###### Ejemplos de fork y signal juntos

En este ejemplo utilizamos un fork para crear un proceso hijo (el padre muere) que responde a signals para realizar ciertas acciones.

	#!/usr/bin/python
	#pansalera
	# exemple fork y signal
	# un pare genera un fill y es mor.
	# El fill va bucle infinit, 
	#  -pero acaba als 3 min
	#  -amb SIGHUP torna a comensar als 3 min
	#  -amb SIGTERM mostra misatge de segons restants i plega.
	#--------------------

	import os,sys, signal

	print os.getpid()

	def sighupHandler(signum,frame):
	  print "Signal handler called with signal:", signum
	  signal.alarm(180)

	def sigtermHandler(signum,frame):
	  print "Signal handler called with signal:", signum
	  print signal.alarm(0)
	  sys.exit(0)


	print "Inici del programa principal PARE"
	pid = os.fork()
	if pid != 0:
	    print "Programa PARE", os.getpid(), pid
	    sys.exit(0)
	else:
	    print "Programa FILL",os.getpid(), pid

	    signal.signal(signal.SIGHUP,sighupHandler)
	    signal.signal(signal.SIGTERM,sigtermHandler)
	    signal.alarm(180)

	    while True:
		# fem un open que es queda encallat
		pass

	sys.exit(0)

El proceso hijo, cuando muere el padre, pasa a tener como padre el proceso systemd(1677) que es el systemd de la sesión de usuario.

	[isx47240077@i04 asixm06-ipc]$ pstree -sp 20169
	systemd(1)───systemd(1677)───python(20169)

Esta variante del ejemplo de fork y signal ejecuta el programa de signals (con es.execv()) que habíamos hecho previamente en vez de tenerlo escrito en el mismo programa.

	#!/usr/bin/python
	#pansalera
	# exemple fork y signal
	# un pare genera un fill y es mor.
	#--------------------
	import os,sys

	print "Inici del programa principal PARE"
	pid = os.fork()
	if pid != 0:
	    #print os.wait()
	    print "Programa PARE", os.getpid(), pid
	else:
	    print "Programa FILL",os.getpid(), pid
	    os.execvp("/usr/bin/python",["/usr/bin/python","/home/users/inf/hisx2/isx47240077/Documents/Escuela/CFGS/SO/UF2/ipc/asixm06-ipc/06-signals.py","3"]) # ejemplo execv

	print "Fi del programa", os.getpid()
	sys.exit(0)

#### Sockets

TTY1

	[isx47240077@i04 ~]$ nc -l 4433 #nc -kl 4433 asi es multiconexion


TTY2

	[isx47240077@i04 ~]$ cat /etc/fstab  | nc localhost 4433

TTY1

	(vuelca el cat)
	#
	# /etc/fstab
	# Created by anaconda on Fri Sep 15 09:52:19 2017
	#
	# Accessible filesystems, by reference, are maintained under '/dev/disk'
	# See man pages fstab(5), findfs(8), mount(8) and/or blkid(8) for more info
	#
	/dev/sda5 /                       ext4    defaults        1 1
	/dev/sda7 swap                    swap    defaults        0 0

	gandhi:/groups/ /home/groups  nfs4  sec=krb5,comment=systemd.automount  0  0

#### Comunicación básica

##### Ejemplo de echo server basic.

Este ejemplo retorna todo lo que recibe y cierra.

	#!/usr/bin/python 
	#-*- coding: utf-8-*-
	# exemple de echo server basic
	# -----------------------------------------------------------------
	# Escola del treball de Barcelona
	# ASIX Hisi2 M06-ASO UF2NF1-Scripts
	# @edt Curs 2017-2018  Febrer 2018
	# -----------------------------------------------------------------
	import sys,socket
	HOST = ''
	PORT = 50001
	s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
	s.bind((HOST, PORT))
	s.listen(1)
	conn, addr = s.accept()
	print 'Connected by', addr
	while True:
	    data = conn.recv(1024)
	    if not data: break
	    conn.send(data)
	conn.close()
	sys.exit(0)

###### Ejemplo de echo server one-by-one

Este ejemplo retorna todo lo que recibe sin cerrar la conexión (sigue a la escucha).

	#!/usr/bin/python 
	#-*- coding: utf-8-*-
	# exemple de echo server basic
	# -----------------------------------------------------------------
	# Escola del treball de Barcelona
	# ASIX Hisi2 M06-ASO UF2NF1-Scripts
	# @edt Curs 2017-2018  Febrer 2018
	# -----------------------------------------------------------------
	import sys,socket
	HOST = ''
	PORT = 50001
	s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
	s.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)	
	s.bind((HOST, PORT))
	s.listen(1)
	conn, addr = s.accept()
	print 'Connected by', addr
	while True:
	    conn,addr = s.accept()
	    while True:
		data = conn.recv(1024)
		if not data: break
		conn.send(data)
	conn.close()
	sys.exit(0)

###### Ejemplo de echo client

(se puede mejorar)

	#!/usr/bin/python 
	#-*- coding: utf-8-*-
	# exemple de echo server basic
	# -----------------------------------------------------------------
	# Escola del treball de Barcelona
	# ASIX Hisi2 M06-ASO UF2NF1-Scripts
	# @edt Curs 2017-2018  Febrer 2017
	# -----------------------------------------------------------------
	import sys,socket
	HOST = ''
	PORT = 50001
	s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
	#s.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
	s.connect((HOST, PORT))
	s.send('Hello, world')
	data = s.recv(1024)
	s.close()
	print 'Received', repr(data)
	sys.exit(0)


###### Ejemplo de echo date basic

	#!/usr/bin/python 
	#-*- coding: utf-8-*-
	# exemple de echo date basic
	# -----------------------------------------------------------------
	# Escola del treball de Barcelona
	# ASIX Hisi2 M06-ASO UF2NF1-Scripts
	# @edt Curs 2017-2018  Febrer 2018
	# -----------------------------------------------------------------
	import sys,socket
	from subprocess import Popen, PIPE
	HOST = ''
	PORT = 50001
	s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
	s.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
	s.bind((HOST, PORT))
	s.listen(1)
	conn,addr = s.accept()
	while True:
	    print "Connected by:", addr
	    command = ["/usr/bin/date"]
	    pipeData = Popen(command, stdout=PIPE, stderr=PIPE)
	    for line in pipeData.stdout:
		conn.send(line)
	    conn.close()
	sys.exit(0)

En el sigiente trozo de código, importamos un trozo de texto del programa echo client para tratarlo de forma interactiva y vemos que (s) es un socket que es diferente al socket (conn).

(s) es un socket que está listen(1) y que cuando hace acceopt() genera el socket (conn) ((s) continua estando listen()).

	[isx47240077@i14 tmp]$ python
	Python 2.7.13 (default, May 10 2017, 20:04:36) 
	[GCC 6.3.1 20161221 (Red Hat 6.3.1-1)] on linux2
	Type "help", "copyright", "credits" or "license" for more information.
	>>> import sys,socket
	>>> from subprocess import Popen, PIPE
	>>> HOST = ''
	>>> PORT = 50001
	>>> s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
	>>> s.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
	>>> s.bind((HOST, PORT))
	>>> s.listen(1)
	>>> conn,addr = s.accept()
	>>> print "Connected by:", addr, s, conn
	Connected by: ('127.0.0.1', 41850) <socket._socketobject object at 0x7ff440a1a750> <socket._socketobject object at 0x7ff440a1a7c0>
	>>> conn.getpeername()
	('127.0.0.1', 41850)
	>>> s.getpeername()
	Traceback (most recent call last):
	  File "<stdin>", line 1, in <module>
	  File "/usr/lib64/python2.7/socket.py", line 228, in meth
	    return getattr(self._sock,name)(*args)
	socket.error: [Errno 107] Transport endpoint is not connected


###### Ejemplo/ejercicio calendar server

(por acabar)

    #!/usr/bin/python 
    #-*- coding: utf-8-*-
    # exemple de echo server basic
    # -----------------------------------------------------------------
    # Escola del treball de Barcelona
    # ASIX Hisi2 M06-ASO UF2NF1-Scripts
    # @edt Curs 2017-2018  Febrer 2018
    # -----------------------------------------------------------------
    
    import sys,socket, argparse, signal, os
    from subprocess import Popen, PIPE
    
    #Fork de programa pare y fill (daemon)
    print "Inici del programa principal PARE"
    pid = os.fork()
    if pid != 0:
      print "Fi del programa principal PARE", os.getpid(), pid
      sys.exit(0)
    
    print "Programa FILL", os.getpid(), pid
    
    #Redefinició de Signals
    def mysigusr1(signum,frame):
        global userList
        print "Signal handler called with signal:", signum
        print "Connected user IPs:\n", userList
    
    def mysigusr2(signum,frame):
        global userList
        print "Signal handler called with signal:", signum
        print "Number of connected users:\n", len(userList)
    
    def mysighup(signum,frame):
        global userList
        print "Signal handler called with signal:", signum
        print "Counters reseted:\n", userList, len(userList)
    
    def mysigalarm(signum,frame):
        print "Signal handler called with signal:", signum
        print "Bye, bye!"
        sys.exit(0)
    
    def mysigterm(signum,frame):
        print "Signal handler called with signal:", signum
        print "I won't close, muahahaha. "
    
    signal.signal(signal.SIGALRM,mysigalarm)
    signal.signal(signal.SIGTERM,mysigterm)
    signal.signal(signal.SIGUSR1,mysigusr1)
    signal.signal(signal.SIGUSR2,mysigusr2)
    signal.signal(signal.SIGHUP,mysighup)
    
    #Definició de variables i constants.
    HOST = ''
    PORT = 50005
    userList = []
    
    # Conexió (sockets).
    s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    s.bind((HOST, PORT))
    s.listen(1)
    
    while True:
      try:
        conn, addr = s.accept()
        userList.append(addr)
      except Exception:
    	continue
      print 'Connected by', addr
      year=conn.recv(1024)
      command = ["/usr/bin/cal -y %s" % year]
      pipeData = Popen(command, shell = True, stdout=PIPE, stderr=PIPE)
      for line in pipeData.stdout:
    	#print line,
      conn.send(line)
    
    conn.close()
    sys.exit(0)


###### Ejemplo/ejercicio calendar client


    # exemple de echo server basic
    # -----------------------------------------------------------------
    # Escola del treball de Barcelona
    # ASIX Hisi2 M06-ASO UF2NF1-Scripts
    # @edt Curs 2017-2018  Febrer 2017
    # -----------------------------------------------------------------
    import sys,socket, argparse
    
    #Gestió d'arguments amb argparse
    parser = argparse.ArgumentParser(description=\
     """Programa que mostra un calendari donat
        un any en concret.""",\
     prog="calendarserver.py")
    parser.add_argument("-s","--server", dest="server", required=True, help="server")
    parser.add_argument("-p","--port", dest="port", type=int, required=True, help="port")
    parser.add_argument("-y","--year", dest="year", required=True, help="year")
    args=parser.parse_args()
    
    HOST = args.server
    PORT = args.port
    YEAR = args.year
    
    s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    s.connect((HOST, PORT))
    s.send(YEAR)
    while True:
        data = s.recv(1024)
        print data
        if not data: break
    s.close()
    sys.exit(0)


###### Ejemplo/ejercicio nmap server

    #!/usr/bin/python 
    #-*- coding: utf-8-*-
    # exemple de echo server basic
    # -----------------------------------------------------------------
    # Escola del treball de Barcelona
    # ASIX Hisi2 M06-ASO UF2NF1-Scripts
    # @edt Curs 2017-2018  Febrer 2018
    # -----------------------------------------------------------------
    
    import sys,socket, time
    from subprocess import Popen, PIPE
    
    #Definició de variables i constants.
    HOST = ''
    PORT = 50005
    
    # Conexió (sockets).
    s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    s.bind((HOST, PORT))
    s.listen(1)
    
    while True:
    
        try:
            conn, addr = s.accept()
        except Exception:
            continue
    
        print 'Connected by', addr
    
        timestamp = time.strftime("%Y-%m-%d-%H-%M-%s")
        filename = "%s-%s.txt" % (addr[0],timestamp)
        ff = open(filename, 'w')
    
        while True:
            data = conn.recv(1024)
            if not data:
                ff.close()
                break
            ff.write(data)
    
        conn.close()
    
    sys.exit(0)
    
**modificación: un fichero por cliente**

	#!/usr/bin/python 
	#-*- coding: utf-8-*-
	# exemple de echo server basic
	# -----------------------------------------------------------------
	# Escola del treball de Barcelona
	# ASIX Hisi2 M06-ASO UF2NF1-Scripts
	# @edt Curs 2017-2018  Febrer 2018
	# -----------------------------------------------------------------

	import sys,socket, time, os
	from subprocess import Popen, PIPE

	#Definició de variables i constants.
	HOST = ''
	PORT = 50008
	ipDic = {}

	# Conexió (sockets).
	s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
	s.bind((HOST, PORT))
	s.listen(1)

	while True:

		try:
		        conn, addr = s.accept()
		except Exception:
		        continue

		print 'Connected by', addr

		timestamp = time.strftime("%Y%m%d%H%M%s")
		filename = "%s-%s.txt" % (addr[0],timestamp)

		if addr[0] in ipDic:
		        os.rename("%s-%s.txt" % (addr[0], ipDic[addr[0]]), "%s-%s.txt" % (addr[0],timestamp))

		ipDic[addr[0]]=timestamp

		print ipDic
		ff = open(filename, 'a')

		while True:
		        data = conn.recv(1024)
		        if not data:
		                ff.close()
		                break
		        ff.write(data)

		conn.close()

	sys.exit(0)


###### Ejemplo/ejercicio nmap client

    nmap localhost | nc localhost 50005

(falta por hacer el programa cliente)

###### Ejemplo/ejercicio telnet server

    #!/usr/bin/python 
    #-*- coding: utf-8-*-
    # exemple de echo server basic
    # -----------------------------------------------------------------
    # Escola del treball de Barcelona
    # ASIX Hisi2 M06-ASO UF2NF1-Scripts
    # @edt Curs 2017-2018  Febrer 2018
    # -----------------------------------------------------------------
    
    import sys,socket, argparse, signal, os
    from subprocess import Popen, PIPE
    
    #Gestió d'arguments amb argparse
    parser = argparse.ArgumentParser(description=\
     """Programa que emula un servidor telnet""",
     prog="telnetserver.py")
    parser.add_argument("-d","--debug", dest="debug", action="store_true", help="debug mode")
    parser.add_argument("-p","--port", dest="port", type=int, required=True, help="port")
    args=parser.parse_args()
    
    
    #Definició de variables i constants.
    HOST = ''
    PORT = args.port
    EOT = chr(4)
    
    # Conexió (sockets).
    s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    s.bind((HOST, PORT))
    s.listen(1)
    
    while True:
        try:
            conn, addr = s.accept()
        except Exception:
            continue
    
        print 'Connected by', addr
        while True:
            command=conn.recv(1024)
            print command
    
            if not command: break
    
            pipeData = Popen(command, shell = True, stdout=PIPE, stderr=PIPE)
            for line in pipeData.stdout:
                if args.debug:
                    print line,
                conn.sendall(line)
            conn.sendall(EOT)
        conn.close()
    sys.exit(0)



###### Ejemplo/ejercicio telnet client

    #!/usr/bin/python 
    #-*- coding: utf-8-*-
    # exemple de telnet client
    # -----------------------------------------------------------------
    # Escola del treball de Barcelona
    # ASIX Hisi2 M06-ASO UF2NF1-Scripts
    # @edt Curs 2017-2018  Febrer 2017
    # -----------------------------------------------------------------
    import sys,socket, argparse
    
    #Gestió d'arguments amb argparse
    parser = argparse.ArgumentParser(description=\
     """Programa que emula un client telnet""",
     prog="telnetserver.py")
    parser.add_argument("-s","--server", dest="server", required=True, help="server")
    parser.add_argument("-p","--port", dest="port", type=int, required=True, help="port")
    args=parser.parse_args()
    
    HOST = args.server
    PORT = args.port
    EOT = chr(4)
    
    s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    s.connect((HOST, PORT))
    
    while True:
        command = raw_input()
        if command == "quit": break
        if not command: continue
        s.send(command)
        while True:
            data = s.recv(1024)
            if data[-1] == EOT:
                print data[:-1]
                break
            print data
    s.close()
    sys.exit(0)
    
    
###### Ejemplo echo server multi-cliente
    
    #!/usr/bin/python
    #-*- coding: utf-8-*-
    '''
    # exemple signal + popen
    # -----------------------------------------------------------------
    # Escola del treball de Barcelona
    # ASIX Hisi2 M06-ASO UF2NF1-Scripts
    # @edt Curs 2014-2015  Desembre 2014
    # Echo server multiple-connexions
    # -----------------------------------------------------------------
    '''
    import socket, sys, select
    
    HOST = ''                 
    PORT = 50005             
    s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    s.bind((HOST, PORT))
    s.listen(1)
    conns=[s]
    
    while True:
        actius,x,y = select.select(conns,[],[]) #esta linea es como un accept, se queda esperando una nueva conexión.
        for actual in actius:
            if actual == s: # ¿es una nueva conexión?
                conn, addr = s.accept() #haciendo el accept, obtenemos el socket conn de la conexión con el cliente.
                print 'Connected by', addr
                conns.append(conn) #Añadimos el socket conn como conexión establecida.
            else: #sino es una nueva conexión, hace lo que haga el server
                data = actual.recv(1024)
                if not data:
                    sys.stdout.write("Client finalitzat: %s \n" % (actual))
                    actual.close()
                    conns.remove(actual) #Eliminamos de la lista de conexiones activas el socket (conexión).
                else:
                    actual.sendall(data)
                    actual.sendall(chr(4),socket.MSG_DONTWAIT)
    s.close()
    sys.exit(0)